if [ ! -z $RUNHOURLY ]; then
    BTCUSD=`cat orange-coin-number.txt`
    USDTHB=`cat daily_rates.json | jq '.quotes.USDTHB'`
    USDTHB=`echo "${BTCUSD} * ${USDTHB}" | bc`
    SATSTHB=`echo "(1 / $USDTHB) * 100000000" | bc -l`
    echo "{\"thb2sats\": $SATSTHB}" > sats2baht.json
fi


