if [ ! -z $RUNDAILY ]; then
    if [ ! -z $CURRENCYLAYER_API ]; then 
        url="http://api.currencylayer.com/live?access_key=${CURRENCYLAYER_API}&currencies=THB,TWD,CZK,USD,HKD,SGD,CAD,AUD,USD,EUR,BTC"

        curl $url 2>/dev/null > daily_rates.json
    fi
fi

